﻿<?php

$a = 0;
$b = 1;
$Erro = 0.00001;
$iteracao = 1;

	function divx($a, $b)
	{		
		return ($a + $b)/2;
	}

	$x = divx($a, $b);
	
	function error($x, $b)
	{
		$res = ($x - $b)/$x;
		return $res;	
	}

	$Error = error($x, $a);

	function fa($a)
	{
		//return (pow($a, 3)) - (2 / $a);
		return (pow($a, 3)) - (9*$a) + 3;
	}

	function fb($b)
	{
		//return (pow($a, 3)) - (2 / $a);
		return (pow($b, 3)) - (9*$b) + 3;
	}

	function fx($x)
	{
		//return (pow($a, 3)) - (2 / $a);
		return (pow($x, 3)) - (9*$x) + 3;
	}

	//$Fa = fa($a);
//echo $Error;
//if($Error > $Erro)
//echo 'Error maior';
//else
//echo 'Erro maior';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Cálculo numérico</title>
<head>
</head>
<body>
<strong>Bem vindo ao meu sistema de cálculo numérico</strong><br />
Será utilizado o método de bissecção<br /><br />

<table border = "1">
	<thead>Bissecção</thead>
	<th>Iterações</th>
	<th>a</th>
	<th>b</th>
	<th>x</th>
	<th>ER(x)</th>
	<th>f(x)</th>
	<th>f(a)</th>
	<th>f(b)</th>
<?php

	while($Error > $Erro)
	{
		$Error = error($x, $a);
?>
<tr>
		<td><?php echo $iteracao; ?></td>
		<td><?php echo $a; ?></td>
		<td><?php echo $b; ?></td>
		<td><?php echo $x; ?></td>
		<td><?php echo $Error; ?></td>		
<?php
		if($Error > $Erro)
		{
		   $Fa = fa($a);
		   $Fb = fb($b);
		   $Fx = fx($x);
		   
		   if(($Fx * $Fa) < 0)
		   {
			$b = $x;
		   }		
		   else
		   {
 			$a = $x;
		   }
		   $x = divx($a, $b);
		   $iteracao++; 	

		}//fecha if
?>
		<td><?php echo floatval($Fx); ?></td>
		<td><?php echo floatval($Fa);?></td>
		<td><?php echo floatval($Fb);?></td>
	</tr>	
<?php
	}//fecha while
?>
</table>


<!--
<form method="post" action="">
Exponecial<br />
<input type="text" name="exp" size="15" /><br />
Número<br />
<input type="text" name="base"  size="15"/><br />
<input type="submit" value="Calcular">
</form>
-->

<body>
</html>
